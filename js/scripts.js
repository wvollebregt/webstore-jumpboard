$(document).ready(function() {
	$('main').accordion({
		header: 'header',
		active: 1,
		event: 'mouseover'
	});

	var d = new Date();
	$('footer').append(' ' + d.getFullYear());

	function buildSandbox(sandboxNo) {
		$('article.sandbox').show();
		$('article.sandbox header .js-sandboxNo-var').text(sandboxNo);
		$('article.sandbox li a').attr('href', function(i, v) {
			return 'http://dev' + sandboxNo + '.web.bestseller.demandware.net' + v;
		});
		Cookies.set('jumpboardSandboxNo', sandboxNo, {
			expires: 365
		});
	}

	if (Cookies.get('jumpboardSandboxNo') == null) {
		Cookies.remove('jumpboardSandboxNo');

		$('.js-popup').dialog({
			modal: true,
			closeText: 'X',
			widget: 'validate',
			show: {
				effect: "blind",
				duration: 800
			},
			buttons: {
				'Set Sandbox server': function() {
					$('.js-popup-input').focus();
					var jsbn = $('.js-popup-input').val();
					if (jsbn.length < 2) {
						alert('Please type at least 2 number-characters. So sandbox 1 = 01');
					} else if (jsbn.length > 2) {
						alert('Please type no more than 2 number-characters');
					} else {
						buildSandbox(jsbn);
						$(this).dialog('close');
					}
				},
				'I am not a developer': function() {
					Cookies.set('jumpboardSandboxNo', 'noDev', {
						expires: 365
					});
					$(this).dialog('close');
					console.log(Cookies.get('jumpboardSandboxNo'));
				}
			}
		});
	} else if (Cookies.get('jumpboardSandboxNo') != 'noDev') {
		var jsbn = Cookies.get('jumpboardSandboxNo');
		buildSandbox(jsbn);
	}
});
